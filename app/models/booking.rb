class Booking < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :date_of_tour, :hotel_name, :pick_up_time, presence: true, length: { maximum: 500 }
  validates :phone_number,presence: true, numericality: { only_integer: true } , length: { is: 11 }
  validates :number_of_pax,presence: true, numericality: { only_integer: true , greater_than_or_equal_to: 1 }
end
