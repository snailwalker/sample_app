class BookingsController < ApplicationController
  before_action :logged_in_user, only: [:show,:edit,:create,:destroy]
  
  def show
    @booking = Booking.find(params[:id])
  end

  def new
    @booking = Booking.new
  end

  def create
    @booking = current_user.bookings.build(booking_params) 
    if @booking.save
      flash[:success] = "You have submited the information successfully!"
      redirect_to root_url
    else
      render 'new'
    end
  end

  def edit
    @booking = Booking.find(params[:id])
  end

  def update
    @booking = Booking.find(params[:id])
    #binding.pry
    if @booking.update_attributes(booking_params)
      flash[:success] = "information updated!"
      redirect_to @booking
    else
      render 'edit'
    end
  end
  
  def destroy
  end
 
  private
  
  def booking_params
    params.require(:booking).permit(:date_of_tour,:hotel_address,:hotel_name,:phone_number,:number_of_pax,:pick_up_time)
  end
end
