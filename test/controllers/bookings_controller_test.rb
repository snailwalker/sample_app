require 'test_helper'

class BookingsControllerTest < ActionController::TestCase
  def setup
    @booking = bookings(:one)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Booking.count' do
      post :create, micropost: { content: "Lorem ipsum" }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Booking.count' do
      delete :destroy, id: @booking
    end
    assert_redirected_to login_url
  end
end