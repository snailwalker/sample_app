require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "Snail Season Travel"
  end

  test "should get help" do
    get :help
    assert_response :success
    assert_select "title", "Help | Snail Season Travel"
  end

  test "should get about" do
  	get :about
  	assert_response :success
    assert_select "title", "About | Snail Season Travel"
  end

  test "should get contact" do
    get :contact
    assert_response :success
    assert_select "title", "Contact | Snail Season Travel"
  end
end