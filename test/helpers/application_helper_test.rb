require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title,         "Snail Season Travel"
    assert_equal full_title("Help"),  "Help | Snail Season Travel"
   end
end