require 'test_helper'

class BookingEditTest < ActionDispatch::IntegrationTest
	def setup
		@user = users(:michael)
		@booking = bookings(:most_recent)
	end

	test "unsucessful edit" do 
	  log_in_as(@user)
	  get edit_booking_path(@booking)
	  assert_template 'bookings/edit'
	  patch booking_path(@booking), booking: {date_of_tour: "2017-05-06", hotel_name: " ", hotel_address:"dadsada das",phone_number:12345678901 , number_of_pax:34 , pick_up_time: "9:00"}
	  assert_template 'bookings/edit'
	end

	test "sucessful edit" do
		log_in_as(@user)
		get edit_booking_path(@booking)
		assert_template 'bookings/edit'
        patch booking_path(@booking), booking: {date_of_tour: "2017-05-06",hotel_name: "cool kids",hotel_address:"dadsada das", phone_number:12345678901 , number_of_pax:34 , pick_up_time: "9:00"}
        # binding.pry
        assert_not flash.empty?
        assert_redirected_to @booking
        @booking.reload
        assert_equal "2017-05-06", @booking.date_of_tour
       assert_equal "cool kids", @booking.hotel_name
    end
end


