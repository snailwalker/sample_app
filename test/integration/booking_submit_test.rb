require 'test_helper'

class BookingSubmitTest < ActionDispatch::IntegrationTest
	def setup
        @user = users(:michael)
    end
	test "invalid booking submit information" do
	 	log_in_as(@user)
	 	get new_booking_path
	    assert_no_difference 'Booking.count' do
	      post bookings_path, booking: {date_of_tour: "2017-05-06", hotel_name: "snail season", phone_number:123456 , number_of_pax:34 , pick_up_time: "9:00"}	
     end
	 assert_template 'bookings/new'
	end

	test "valid booking submit information" do
		log_in_as(@user)
		get new_booking_path
		assert_difference 'Booking.count', 1 do
			post bookings_path, booking: {date_of_tour: "2017-05-06", hotel_name: "snail season", phone_number:12345678901 , number_of_pax:34 , pick_up_time: "9:00"}
		end
		assert_redirected_to root_url
	end
end

