require 'test_helper'

class BookingTest < ActiveSupport::TestCase
	def setup
		@user = users(:michael)
		@booking = @user.bookings.build( date_of_tour: "2017-05-06", hotel_name: "snail season", phone_number:12345678901 , number_of_pax:34 , pick_up_time: "9:00" ) 
	end

	test "should be valid" do
		assert @booking.valid?
	end

	test "user id should be present" do
        @booking.user_id = nil
        assert_not @booking.valid?
    end


	test "date_of_tour should be present" do
	    @booking.date_of_tour = " "
	    assert_not @booking.valid?
	end

	test "hotel_name should be present" do
	    @booking.hotel_name = " "
	    assert_not @booking.valid?
	end

	test "phone_number" do
	    @booking.phone_number = " "
	    assert_not @booking.valid?
	    @booking.phone_number = "132ssdsd"
	    assert_not @booking.valid?
	    @booking.phone_number = 90
	    assert_not @booking.valid?
	    @booking.phone_number =12348576123
	    assert @booking.valid?
	end

	test "number_of_pax" do
	    @booking.number_of_pax = " "
	    assert_not @booking.valid?
	    @booking.number_of_pax =0
	    assert_not @booking.valid?
	    @booking.number_of_pax =99
	    assert @booking.valid?
	end
    
    test "number_of_pax range" do
        @booking.number_of_pax =111
	    assert @booking.valid?
	end
	
	test "pick_up_time should be present" do
	    @booking.pick_up_time = " "
	    assert_not @booking.valid?
	end

	test "order should be most recent first" do
    assert_equal bookings(:most_recent), Booking.first
  end
end
