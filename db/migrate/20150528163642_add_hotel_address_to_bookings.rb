class AddHotelAddressToBookings < ActiveRecord::Migration
  def change
  	 add_column :bookings, :hotel_address, :string
  end
end
