class ChangeDataTypeForPhonenumber < ActiveRecord::Migration
  def self.up
  	change_column :bookings, :phone_number, :bigint
  end

  def down
  	change_column :bookings, :phone_number, :integer 
  end
end