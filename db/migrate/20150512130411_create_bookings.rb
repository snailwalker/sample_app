class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.string :date_of_tour
      t.string :hotel_name
      t.integer :phone_number
      t.integer :number_of_pax
      t.string :pick_up_time

      t.timestamps null: false
    end
  end
end
